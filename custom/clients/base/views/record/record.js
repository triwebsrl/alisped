
/*
 * Your installation or use of this SugarCRM file is subject to the applicable
 * terms available at
 * http://support.sugarcrm.com/06_Customer_Center/10_Master_Subscription_Agreements/.
 * If you do not agree to all of the applicable terms or do not have the
 * authority to bind the entity as an authorized representative, then do not
 * install or use this SugarCRM file.
 *
 * Copyright (C) SugarCRM Inc. All rights reserved.
 */
({

    extendsFrom: 'RecordView',

    /**
     */
    initialize: function(options) {
        this.plugins = _.union(this.plugins || [], ['HistoricalSummary']);
        this._super('initialize', [options]);
    },


    events: {
        'change input[name=\'transport_type\']': function() { this.tw_dependency_rese(); },
    },

    tw_module: 'FWD_rese',
    /*
    * Edit view Visibilità dipendente dal campo transport_type per i campi relazionati Area Mare Terra
    * */
    tw_dependency_rese: function () {

           if(this.module==this.tw_module) {


            if($('input[name="transport_type"]').val()==""){
              $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();
            }

            //AEREO 
            if($('input[name="transport_type"]').val()=="AEX" || $('input[name="transport_type"]').val()=="EXPRESS_COURIER" || $('input[name="transport_type"]').val()=="AIM" ){

               $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
               $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

               $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
               $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

               $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").show();
               $("div[data-name='tw_aereo_fwd_rese_name'] .normal").show();
            }


               //MARE 
               if($('input[name="transport_type"]').val()=="MEX_LCL" || 
                 $('input[name="transport_type"]').val()=="MEX_FCL" || 
                 $('input[name="transport_type"]').val()=="MIM_FCL" || 
                 $('input[name="transport_type"]').val()=="MIM_LCL"){

                  $("div[data-name='tw_mare_fwd_rese_name'] .record-label").show();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").show();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();

          }

               //TERRA 
               if($('input[name="transport_type"]').val()=="TEX_GROUPAGE" || 
                 $('input[name="transport_type"]').val()=="TIM_GROUPAGE" ){

                  $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").show();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").show();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();

          }

         }
    },

   /*
    * Detail view Visibilità dipendente dal campo transport_type per i campi relazionati Area Mare Terra  
    * */
    tw_dependency_rese_detail: function () {

           if(this.module==this.tw_module) {

              $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();


            //AEREO 
            if(this.model.get('transport_type')=="AEX" || this.model.get('transport_type')=="EXPRESS_COURIER" || this.model.get('transport_type')=="AIM" ){

               $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
               $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

               $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
               $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

               $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").show();
               $("div[data-name='tw_aereo_fwd_rese_name'] .normal").show();

           }

               //MARE 
               if(this.model.get('transport_type')=="MEX_LCL" || 
                 this.model.get('transport_type')=="MEX_FCL" || 
                 this.model.get('transport_type')=="MIM_FCL" || 
                 this.model.get('transport_type')=="MIM_LCL"){

                  $("div[data-name='tw_mare_fwd_rese_name'] .record-label").show();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").show();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();

          }

               //TERRA 
               if(this.model.get('transport_type')=="TEX_GROUPAGE" || 
                 this.model.get('transport_type')=="TIM_GROUPAGE" ){

                  $("div[data-name='tw_mare_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_mare_fwd_rese_name'] .normal").hide();

              $("div[data-name='tw_terra_fwd_rese_name'] .record-label").show();
              $("div[data-name='tw_terra_fwd_rese_name'] .normal").show();

              $("div[data-name='tw_aereo_fwd_rese_name'] .record-label").hide();
              $("div[data-name='tw_aereo_fwd_rese_name'] .normal").hide();

          }

      }


  },
})
