<?php
 $dependencies['FWD_rese']['required_tw_terra_fwd_rese_name'] = array(
        'hooks' => array("edit"),
        //Optional, the trigger for the dependency. Defaults to 'true'.
        'trigger' => 'true', 
        'triggerFields' => array('transport_type'),
        'onload' => true,
        //Actions is a list of actions to fire when the trigger is true
        'actions' => array(
            array(
                'name' => 'SetRequired',
                //The parameters passed in will depend on the action type set in 'name'
                'params' => array(
                    'target' => 'tw_terra_fwd_rese_name',
                    //id of the label to add the required symbol to
                    'label' => 'tw_terra_fwd_rese_name_label',
                    //Set required if the status is closed
                    'value' => 'or(equal($transport_type, "TEX_GROUPAGE"),equal($transport_type, "TIM_GROUPAGE"))' 
                )
            ),

        ),
        //Actions fire if the trigger is false. Optional.
        'notActions' => array(),
   );

//or(equal($transport_type, "AEX"),equal($transport_type, "EXPRESS_COURIER"))


